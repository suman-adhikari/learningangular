import { Injectable } from '@angular/core';
import {InMemoryDbService} from 'angular-in-memory-web-api';
import { Hero } from './hero';

@Injectable({
  providedIn: 'root'
})
export class InMemoryDataService implements InMemoryDbService{

    createDb(){
      const heroes = [
                  { id: 19, name: 'Mason Mount', description: 'Best young midfield from academy' },
                  { id: 26, name: 'Ceaser Apziquilata', description: 'The awesome, my reliable, our captain' },
                  { id: 24, name: 'James', description: 'Best young defender from academy' },
                  { id: 5, name: 'Jourgihno', description: 'Easy to overlook but is the best UEFA player 2021' },
                  { id: 7, name: 'N\'golo Kante', description: '25% is land , rest is covered by kante' },
                  { id: 10, name: 'Christian Pulasic', description: 'The Captain America goes for direct forward run' },
                  { id: 29, name: 'Kai', description: 'Yet another young star, scored the only goal in UEFA champions league' }
      ];

      return {heroes};
    }

  // Overrides the genId method to ensure that a hero always has an id.
  // If the heroes array is empty,
  // the method below returns the initial number (11).
  // if the heroes array is not empty, the method below returns the highest
  // hero id + 1.
  genId(heroes: Hero[]): number {
    return heroes.length > 0 ? Math.max(...heroes.map(hero => hero.id)) + 1 : 11;
  }
 
}
