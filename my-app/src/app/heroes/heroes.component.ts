import { Component, OnInit } from '@angular/core';
import { Hero } from '../hero';
import { HeroService } from '../hero.service';
import { MessageService } from '../message.service';

@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.css']
})
export class HeroesComponent implements OnInit {

  heroes : Hero[] = [];

  constructor(private heroservice : HeroService, private messageService: MessageService) { }

  ngOnInit(): void {
    this.getHeroes()
  }

  getHeroes(): void{
    this.heroservice.getHeroes().subscribe(heroes => this.heroes = heroes);
  }

  Add(name:string, id:string, desc: string):void{
    const hero={ id: Number(id), name:name, description:desc}
    if(!name) return;
      this.heroservice.AddHero(hero as Hero).subscribe(hero=> this.heroes.push(hero))
  }

  Delete(hero:Hero){
    this.heroes = this.heroes.filter(h => h != hero )
    this.heroservice.DeleteHero(hero.id).subscribe();
  }

}
