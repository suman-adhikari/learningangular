import { Hero } from "./hero";

export const HEROES: Hero[] = [

    { id: 19, name: 'Mason Mount', description: 'Best young midfield from academy' },
    { id: 26, name: 'Ceaser Apziquilata', description: 'The awesome, my reliable, our captain' },
    { id: 24, name: 'James', description: 'Best young defender from academy' },
    { id: 5, name: 'Jourgihno', description: 'Easy to overlook but is the best UEFA player 2021' },
    { id: 7, name: 'N\'golo Kante', description: '25% is land , rest is covered by kante' },
    { id: 10, name: 'Christian Pulasic', description: 'The Captain America goes for direct forward run' },
    { id: 29, name: 'Kai', description: 'Yet another young star, scored the only goal in UEFA champions league' }

]