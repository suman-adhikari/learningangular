import { Injectable } from '@angular/core';
import {Hero} from './hero'
import { Observable, of } from 'rxjs';
import { MessageService } from './message.service'; 
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})

export class HeroService{
 
  private heroesUrl = 'api/heroes';

  private httpOption ={
    headers : new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(private http: HttpClient, private messageService : MessageService) { }

  getHeroes():Observable<Hero[]>{
    return this.http.get<Hero[]>(this.heroesUrl)
           .pipe(
              catchError(this.ErrorHandler<Hero[]>("getHeroes",[])),
              tap(_ => this.log("fetching heroes"))
           );
  }

  private log(message:string):void{
     this.messageService.add(`HeroService: ${message}`); 
  }

  getHero(id:number):Observable<Hero>{
    const url = `${this.heroesUrl}/${id}`;
    return this.http.get<Hero>(url)
            .pipe(
              tap(_ => this.log(`Fetching hero with id=${id}`)),
              catchError(this.ErrorHandler<Hero>(`getHero(${id})`))
            )
  }

  updateHero(hero:Hero):Observable<any>{
    return this.http.put(this.heroesUrl, hero, this.httpOption)
            .pipe(
              tap(_ => this.log(`${hero.name} updated`)),
              catchError(this.ErrorHandler<any>('updateHero'))
            )
  }

  AddHero(hero:Hero):Observable<Hero>{
    return this.http.post<Hero>(this.heroesUrl, hero, this.httpOption)
            .pipe(
              tap((newHero:Hero)=> this.log(`${newHero.name} new hero added`)),
              catchError(this.ErrorHandler<Hero>('AddHero'))
            )
  }

  DeleteHero(id: number):Observable<Hero>{
    const url= `${this.heroesUrl}/${id}`;
    return this.http.delete<Hero>(url, this.httpOption)
            .pipe(
              tap(_=> this.log(`deleting hero id=${id}`)),
              catchError(this.ErrorHandler<Hero>('DeleteHero'))
            )
  }

  private ErrorHandler<T>(operation="operation", result?:T){
    return (error: any): Observable<T> => {
      
      console.error(error);

      this.log(`${operation} failed: ${error.body.error}`);

      return of(result as T);
    }
  }

}
