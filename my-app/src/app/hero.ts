export interface Hero {
    id: number; //strong typing- the id should be numeric value only
    name: string;
    description: string;
}