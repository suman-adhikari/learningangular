import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MessageService {

  message : string[] = [];
  constructor() { }

  add(msg:string):void{
      this.message.push(msg);
  }

  clear():void{
    this.message=[];  
  }

   getMessage():Observable<string[]>{
     return of(this.message)
   }

}
